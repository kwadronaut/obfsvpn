module 0xacab.org/leap/obfsvpn

go 1.17

require (
	git.torproject.org/pluggable-transports/goptlib.git v1.0.0
	github.com/kalikaneko/socks5 v1.0.1
	github.com/xtaci/kcp-go v5.4.20+incompatible
	// Do not update obfs4 past e330d1b7024b, a backwards incompatible change was
	// made that will break negotiation.
	gitlab.com/yawning/obfs4.git v0.0.0-20210511220700-e330d1b7024b
)

require (
	github.com/dchest/siphash v1.2.1 // indirect
	github.com/klauspost/cpuid/v2 v2.0.6 // indirect
	github.com/klauspost/reedsolomon v1.9.16 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/templexxx/cpufeat v0.0.0-20180724012125-cef66df7f161 // indirect
	github.com/templexxx/xor v0.0.0-20191217153810-f85b25db303b // indirect
	github.com/tjfoc/gmsm v1.4.1 // indirect
	github.com/xtaci/lossyconn v0.0.0-20200209145036-adba10fffc37 // indirect
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97 // indirect
	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d // indirect
	golang.org/x/sys v0.0.0-20210809222454-d867a43fc93e // indirect
)
