package main

import (
	"flag"
	"io"
	"log"
	"net"
	"os"

	"0xacab.org/leap/obfsvpn/client"
)

func main() {
	// Setup logging.
	logger := log.New(os.Stderr, "", log.LstdFlags)
	debug := log.New(io.Discard, "DEBUG ", log.LstdFlags)

	// setup command line flags.
	var (
		verbose     bool
		obfs4Cert   string
		obfs4Remote string
		socksPort   string
		socksHost   string
	)

	socksPort = "8080"
	socksHost = "127.0.0.1"

	flags := flag.NewFlagSet(os.Args[0], flag.ContinueOnError)
	flags.BoolVar(&verbose, "v", verbose, "Enable verbose logging")
	flags.StringVar(&obfs4Cert, "c", obfs4Cert, "The remote obfs4 certificate")
	flags.StringVar(&obfs4Remote, "r", obfs4Remote, "The remote obfs4 endpoint (ip:port)")
	flags.StringVar(&socksPort, "p", socksPort, "The port for the local socks5 proxy (default: 8080)")
	flags.StringVar(&socksHost, "i", socksHost, "The host for the local socks5 proxy (default: localhost)")
	err := flags.Parse(os.Args[1:])
	if err != nil {
		logger.Fatalf("error parsing flags: %v", err)
	}
	// Configure logging.
	if verbose {
		debug.SetOutput(os.Stderr)
	}

	kcpTransport := false
	// TODO make this configurable via a Config struct
	// TODO make sure we're disabling all the crypto options for KCP
	if os.Getenv("KCP") == "1" {
		kcpTransport = true
	}

	socksAddr := net.JoinHostPort(socksHost, socksPort)
	c := client.NewClient(kcpTransport, socksAddr, obfs4Cert)
	_, err = c.Start()
	if err != nil {
		log.Fatal(err)
	}
}
