#!/bin/sh
set -x
sudo openvpn \
    --verb 3 \
    --tls-cipher DHE-RSA-AES128-SHA \
    --cipher AES-128-CBC \
    --auth-nocache \
    --proto tcp \
    --dev tun --client --tls-client \
    --remote-cert-tls server --tls-version-min 1.2 \
    --ca /tmp/ca.crt --cert /tmp/cert.pem --key /tmp/cert.pem \
    --pull-filter ignore ifconfig-ipv6 \
    --pull-filter ignore route-ipv6 \
    --socks-proxy 127.0.0.1 8080 \
    --remote $OBFS4_ENDPOINT_IP $OBFS4_ENDPOINT_PORT \
    --route $OBFS4_ENDPOINT_IP 255.255.255.255 net_gateway
